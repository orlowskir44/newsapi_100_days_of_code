from datetime import *
import requests
from twilio.rest import Client
import config as c

STOCK = "TSLA"
COMPANY_NAME = "Tesla Inc"

Y_FORMAT = date.today() - timedelta(days=1)
BY_FORMAT = date.today() - timedelta(days=2)
# 1 API:
ALP_URL = 'https://www.alphavantage.co/query?'
ALP_PARAMETERS = {
    "function": "TIME_SERIES_DAILY",
    "apikey": c.ALP_KEY,
    "symbol": STOCK
}
# 2 API:
NEWS_URL = "https://newsapi.org/v2/everything?q=teslainc"
NEWS_PARAMETERS = {
    "from": Y_FORMAT,
    "to": BY_FORMAT,
    "sortBy": "publishedAt",
    "apiKey": c.NEWS_KEY
}
# 3 TWI:
account_sid = 'AC842c02c1fb8f047337dc9388f30e9a0c'
auth_token = c.AUTH_TOKEN

# ----------
alp_request = requests.get(ALP_URL, params=ALP_PARAMETERS)
alp_request.raise_for_status()
data = alp_request.json()['Time Series (Daily)']

yesterday = data[str(Y_FORMAT)]['4. close']
b_yesterday = data[str(BY_FORMAT)]['4. close']


STOCK_PRICE = (float(yesterday) - float(b_yesterday))
DIFF_PRECENT = round(STOCK_PRICE / float(b_yesterday) * 100)

if abs(STOCK_PRICE) >= 5:
    news_request = requests.get(url=NEWS_URL, params=NEWS_PARAMETERS)
    news_request.raise_for_status()
    news_info = news_request.json()['articles'][:3]

    three_news = [f"{STOCK}: {round(STOCK_PRICE)} ({(abs(DIFF_PRECENT))}%)" \
                  f"\nHeadline: {article['title']}. \nBrief:{article['description']}" for article in news_info]

    for article in three_news:
        client = Client(account_sid, auth_token)
        message = client.messages.create(
            from_=c.T_NUMBER,
            body=article,
            to=c.MY_NUMBER
        )